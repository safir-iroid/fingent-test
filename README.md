## How to use

clone Instructions

- Clone the repository with __git clone__
- Copy __.env.example__ file to __.env__
- Edit database credentials in __.env__
- Run __composer install__
- Run __php artisan key:generate__
- Run __php artisan migrate__
- Run __php artisan db:seed__
- Run __php artisan serve__  


## User credentials to use after database seeding

Username : admin@admin.com and password : 123456

