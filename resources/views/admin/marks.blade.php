@extends('admin.layouts.app')
@section('content')
<div class="container-xxl flex-grow-1 container-p-y">
    <h4 class="fw-bold py-1 mb-1">Marks </h4>
    <div class="row">
        <div class="col-md-12"> 
            <div class="card mb-4">
                
                @if(isset($mark))
                <h5 class="card-header">EDIT</h5>
                @else
                <h5 class="card-header">ADD NEW</h5>
                @endif
                <!-- Account -->
                <div class="card-body">
                    @if (\Session::has('msg'))
                        <div class="alert alert-success">
                            {!! \Session::get('msg') !!}</li> 
                        </div>
                    @endif
                    @if(isset($mark))
                        {{ Form::model($mark, ['route' => ['marks.update', $mark->id], 'method' => 'patch']) }}
                        @csrf
                        <div class="row">
                            <div class="row">
                                <div class="mb-3 col-md-6">
                                    <label for="name" class="form-label"> Name</label>
                                     <select name="student_id" id="" class="form-control">
                                        <option value="">-Select a Student-</option>
                                        @foreach ($students as $student)
                                            <option value="{{$student->id}}" selected>{{$student->name}}</option>
                                        @endforeach
                                     </select>
                                </div>
                                <div class="mb-3 col-md-6">
                                    <label for="name" class="form-label"> Term</label>
                                    <select name="term_id" id="" class="form-control">
                                        <option value="">-Select a Term-</option>
                                        @foreach ($terms as $term)
                                            <option value="{{$term->id}}" selected>{{$term->name}}</option>
                                        @endforeach
                                     </select>
                                </div>
                                
                                <div class="card">
                                    <div class="card-title">
                                        MARKS
                                    </div>
                                    <table class="table table-striped table-dark table-bordered">
                                    <thead>
                                      <tr>
                                        <th scope="col">SUBJECTS</th>
                                        <th scope="col">MARKS</th> 
                                      </tr>
                                    </thead>
                                    <tbody>
                                        @foreach ($subjects as $subject)
                                            <tr>
                                                <th scope="row"><input type="hidden" class="form-control subject_id" name="subject_id[]" value="{{$subject->id}}">{{$subject->name}}</th>
                                                <td><input type="text"  name="marks[]" class="form-control marks" value=""></td> 
                                            </tr>
                                        @endforeach 
                                    </tbody>
                                  </table>
                                </div> 
                                </div> 
                            </div> 
                            <div class="mt-2">
                                <button type="submit" onClick="$(this).text('Loading..')" class="btn btn-primary me-2">Save</button> 
                                <a  href="{{ route('marks.index') }}" class="btn btn-danger me-2">Cancel</a>
                            </div>
                        </div> 
                        {{ Form::close() }}
                    @else 
                    <form id="formAccountSettings" method="POST" class=".markEntryForm">
                        @csrf
                        <div class="row">
                            <div class="mb-3 col-md-6">
                                <label for="name" class="form-label"> Name</label>
                                 <select name="student_id" id="" class="form-control">
                                    <option value="">-Select a Student-</option>
                                    @foreach ($students as $student)
                                        <option value="{{$student->id}}" selected>{{$student->name}}</option>
                                    @endforeach
                                 </select>
                            </div>
                            <div class="mb-3 col-md-6">
                                <label for="name" class="form-label"> Term</label>
                                <select name="term_id" id="" class="form-control">
                                    <option value="">-Select a Term-</option>
                                    @foreach ($terms as $term)
                                        <option value="{{$term->id}}" selected>{{$term->name}}</option>
                                    @endforeach
                                 </select>
                            </div>
                            
                            <div class="card">
                                <div class="card-title">
                                    MARKS
                                </div>
                                <table class="table table-striped table-dark table-bordered">
                                <thead>
                                  <tr>
                                    <th scope="col">SUBJECTS</th>
                                    <th scope="col">MARKS</th> 
                                  </tr>
                                </thead>
                                <tbody>
                                    @foreach ($subjects as $subject)
                                        <tr>
                                            <th scope="row"><input type="hidden" class="form-control subject_id" name="subject_id[]" value="{{$subject->id}}">{{$subject->name}}</th>
                                            <td><input type="text"  name="marks[]" class="form-control marks" value=""></td> 
                                        </tr>
                                    @endforeach 
                                </tbody>
                              </table>
                            </div>
                                
                                 
                            </div>
                            <div class="mt-2">
                                <button type="submit" class="btn btn-primary me-2 createMarkButton">Save</button> 
                            </div>
                        </div> 
                    </form>
                    @endif
                </div>
                <hr class="my-0" />
                <div class="card-body">
                    <div class="table-responsive text-nowrap">
                        <table class="table table-bordered">
                            <thead class="table-dark">
                                <tr> 
                                    <th class="text-white">ID</th> 
                                    <th class="text-white">NAME</th> 
                                    @foreach ($subjects as $subject)
                                    <th class="text-white">{{$subject->name}}</th>   
                                    @endforeach
                                    <th class="text-white">Total</th>
                                    <th class="text-white">Actions</th>
                                </tr>
                            </thead>
                            <tbody class="table-border-bottom-0 markLoopSection">
                                @foreach ($marksList as $mark)
                                <tr> 
                                    <td>{{ $mark->id }}</td>
                                    <td>{{ $mark->student->name }}</td>
                                    @php $total=0; @endphp
                                    @foreach($mark->mark_items as $item) 
                                    @php $total = isset($item->marks) ? $total+($item->marks) :'' @endphp
                                    <td>{{isset($item->marks)?$item->marks:''}}</td>
                                    @endforeach
                                    <td>{{$total}}</td>
                                    <td>
                                        <a class="btn btn-sm btn-primary text-white" href="{{ route('marks.edit',$mark->id )}}" ><i class="bx bx-edit-alt me-1"></i> Edit</a >
                                        <a class="btn btn-sm btn-danger text-white deleteActionbutton" data-table="marks" data-id="{{ $mark->id }}"><i class="bx bx-trash me-1"></i> Delete</a >
                                    </td>
                                </tr>
                                @endforeach
                            </tbody>
                        </table>
                    </div>
                </div>
                <!-- /Account -->
            </div>
            
        </div>
    </div>
</div> 
@endsection
