@component('admin.component.toaster')
@slot('toaster_id')  @endslot
@slot('toaster_title')   @endslot
@slot('toaster_content')  @endslot
@endcomponent