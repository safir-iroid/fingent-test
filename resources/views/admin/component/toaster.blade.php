 <!-- Toast with Placements -->
 <div id="{{ $toaster_id }}" class="bs-toast toast toast-placement-ex m-2"  role="alert" aria-live="assertive" aria-atomic="true" data-delay="2000" >
 <div class="toast-header">
   <i class="bx bx-bell me-2"></i>
   <div class="me-auto fw-semibold" id="toaster_title">{{ $toaster_title }}</div> 
   <button type="button" class="btn-close" data-bs-dismiss="toast" aria-label="Close"></button>
 </div>
 <div class="toast-body"  id="toaster_content">{{ $toaster_content }}</div>
</div>
<!-- Toast with Placements -->