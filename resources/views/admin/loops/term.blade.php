@forelse ($termsList as $term)
<tr> 
    <td>{{ $term->name }}</td> 
    <td>
        <a class="btn btn-sm btn-primary text-white" href="{{ route('terms.edit',$term->id )}}" ><i class="bx bx-edit-alt me-1"></i> Edit</a >
        <a class="btn btn-sm btn-danger text-white deleteActionbutton" data-table="terms" data-id="{{ $term->id }}"><i class="bx bx-trash me-1"></i> Delete</a >
    </td>
</tr>
@empty
<tr> 
    <td colspan="2" class="emptyTd text-center"><svg xmlns="http://www.w3.org/2000/svg" width="25" height="25"  style="fill: rgb(80, 78, 78);transform: ;msFilter:;"><path d="M11.953 2C6.465 2 2 6.486 2 12s4.486 10 10 10 10-4.486 10-10S17.493 2 11.953 2zM12 20c-4.411 0-8-3.589-8-8s3.567-8 7.953-8C16.391 4 20 7.589 20 12s-3.589 8-8 8z"></path><path d="M11 7h2v7h-2zm0 8h2v2h-2z"></path></svg> No related records found ! </td>     
</tr>
@endforelse  