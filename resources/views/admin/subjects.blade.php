@extends('admin.layouts.app')
@section('content')
<div class="container-xxl flex-grow-1 container-p-y">
    <h4 class="fw-bold py-3 mb-4"><span class="text-muted fw-light">Settings /</span> Subjects</h4>
    <div class="row">
        <div class="col-md-12">
            @include('admin.layouts.nav_items')
            <div class="card mb-4">
                
                @if(isset($subject))
                <h5 class="card-header">EDIT</h5>
                @else
                <h5 class="card-header">ADD NEW</h5>
                @endif
                <!-- Account -->
                <div class="card-body">
                    @if (\Session::has('msg'))
                        <div class="alert alert-success">
                            {!! \Session::get('msg') !!}</li> 
                        </div>
                    @endif
                    @if(isset($subject))
                        {{ Form::model($subject, ['route' => ['subjects.update', $subject->id], 'method' => 'patch']) }}
                        <div class="row">
                            <div class="mb-3 col-md-6">
                                <label for="name" class="form-label"> Name</label>
                                <input class="form-control" type="text" id="name" name="subject" value="{{ $subject->name }}" placeholder="Enter Name" autofocus />
                            </div>
                            <div class="mt-2">
                                <button type="submit" onClick="$(this).text('Loading..')" class="btn btn-primary me-2">Save</button> 
                                <a  href="{{ route('subjects.index') }}" class="btn btn-danger me-2">Cancel</a>
                            </div>
                        </div> 
                        {{ Form::close() }}
                    @else 
                    <form id="formAccountSettings" method="POST" >
                        <div class="row">
                            <div class="mb-3 col-md-6">
                                <label for="name" class="form-label"> Name</label>
                                <input class="form-control" type="text" id="name" name="name" value="" placeholder="Enter Name" autofocus />
                            </div>
                            <div class="mt-2">
                                <button type="submit" class="btn btn-primary me-2 createSubjectButton">Save</button> 
                            </div>
                        </div> 
                    </form>
                    @endif
                </div>
                <hr class="my-0" />
                <div class="card-body">
                    <div class="table-responsive text-nowrap">
                        <table class="table table-bordered">
                            <thead class="table-dark">
                                <tr> 
                                    <th class="text-white">subject</th> 
                                    <th class="text-white">Actions</th>
                                </tr>
                            </thead>
                            <tbody class="table-border-bottom-0 subjectLoopSection">
                                @include('admin.loops.subject') 
                            </tbody>
                        </table>
                    </div>
                </div>
                <!-- /Account -->
            </div>
            
        </div>
    </div>
</div> 
@endsection
