<!DOCTYPE html> 
<html
    lang="en"
    class="light-style layout-menu-fixed"
    dir="ltr"
    data-theme="theme-default"
    data-asset-path=""
    data-template="vertical-menu-template-free"
    >
    <head>
        <meta charset="utf-8" />
        <meta
            name="viewport"
            content="width=device-width, initial-scale=1.0, user-scalable=no, minimum-scale=1.0, maximum-scale=1.0"
            />
        <title>Dashboard - Analytics | Sneat - Bootstrap 5 HTML Admin Template - Pro</title>
        <meta name="description" content="" />
        <!-- Favicon -->
        <link rel="icon" type="image/x-icon" href="{!! asset('dashboard/assets/img/favicon/favicon.ico') !!}" />
        <!-- Fonts -->
        <link rel="preconnect" href="https://fonts.googleapis.com" />
        <link rel="preconnect" href="https://fonts.gstatic.com" crossorigin />
        <link
            href="https://fonts.googleapis.com/css2?family=Public+Sans:ital,wght@0,300;0,400;0,500;0,600;0,700;1,300;1,400;1,500;1,600;1,700&display=swap"
            rel="stylesheet"
            />
        <!-- Icons. Uncomment required icon fonts -->
        <link rel="stylesheet" href="{!! asset('dashboard/assets/vendor/fonts/boxicons.css') !!}" />
        <!-- Core CSS -->
        <link rel="stylesheet" href="{!! asset('dashboard/assets/vendor/css/core.css" class="template-customizer-core-css') !!}" />
        <link rel="stylesheet" href="{!! asset('dashboard/assets/vendor/css/theme-default.css" class="template-customizer-theme-css') !!}" />
        <link rel="stylesheet" href="{!! asset('dashboard/assets/css/demo.css') !!}" />
        <!-- Vendors CSS -->
        <link rel="stylesheet" href="{!! asset('dashboard/assets/vendor/libs/perfect-scrollbar/perfect-scrollbar.css') !!}" />
        <link rel="stylesheet" href="{!! asset('dashboard/assets/vendor/libs/apex-charts/apex-charts.css') !!}" />
        <!-- Page CSS -->
        <!-- Helpers -->
        <script src="{!! asset('dashboard/assets/vendor/js/helpers.js') !!}"></script>
        <!--! Template customizer & Theme config files MUST be included after core stylesheets and helpers.js in the <head> section -->
        <!--? Config:  Mandatory theme config file contain global vars & default theme options, Set your preferred theme option in this file.  -->
        <script src="{!! asset('dashboard/assets/js/config.js') !!}"></script>
    </head>
    <body>
        <!-- Layout wrapper -->
        <div class="layout-wrapper layout-content-navbar">
            <div class="layout-container">
                <!-- Menu -->
                <aside id="layout-menu" class="layout-menu menu-vertical menu bg-menu-theme">
                    <div class="app-brand demo">
                        <a href="index.html" class="app-brand-link">
                            <span class="app-brand-logo demo">
                                <svg
                                    width="25"
                                    viewBox="0 0 25 42"
                                    version="1.1"
                                    xmlns="http://www.w3.org/2000/svg"
                                    xmlns:xlink="http://www.w3.org/1999/xlink"
                                    >
                                    <defs>
                                        <path
                                            d="M13.7918663,0.358365126 L3.39788168,7.44174259 C0.566865006,9.69408886 -0.379795268,12.4788597 0.557900856,15.7960551 C0.68998853,16.2305145 1.09562888,17.7872135 3.12357076,19.2293357 C3.8146334,19.7207684 5.32369333,20.3834223 7.65075054,21.2172976 L7.59773219,21.2525164 L2.63468769,24.5493413 C0.445452254,26.3002124 0.0884951797,28.5083815 1.56381646,31.1738486 C2.83770406,32.8170431 5.20850219,33.2640127 7.09180128,32.5391577 C8.347334,32.0559211 11.4559176,30.0011079 16.4175519,26.3747182 C18.0338572,24.4997857 18.6973423,22.4544883 18.4080071,20.2388261 C17.963753,17.5346866 16.1776345,15.5799961 13.0496516,14.3747546 L10.9194936,13.4715819 L18.6192054,7.984237 L13.7918663,0.358365126 Z"
                                            id="path-1"
                                            ></path>
                                        <path
                                            d="M5.47320593,6.00457225 C4.05321814,8.216144 4.36334763,10.0722806 6.40359441,11.5729822 C8.61520715,12.571656 10.0999176,13.2171421 10.8577257,13.5094407 L15.5088241,14.433041 L18.6192054,7.984237 C15.5364148,3.11535317 13.9273018,0.573395879 13.7918663,0.358365126 C13.5790555,0.511491653 10.8061687,2.3935607 5.47320593,6.00457225 Z"
                                            id="path-3"
                                            ></path>
                                        <path
                                            d="M7.50063644,21.2294429 L12.3234468,23.3159332 C14.1688022,24.7579751 14.397098,26.4880487 13.008334,28.506154 C11.6195701,30.5242593 10.3099883,31.790241 9.07958868,32.3040991 C5.78142938,33.4346997 4.13234973,34 4.13234973,34 C4.13234973,34 2.75489982,33.0538207 2.37032616e-14,31.1614621 C-0.55822714,27.8186216 -0.55822714,26.0572515 -4.05231404e-15,25.8773518 C0.83734071,25.6075023 2.77988457,22.8248993 3.3049379,22.52991 C3.65497346,22.3332504 5.05353963,21.8997614 7.50063644,21.2294429 Z"
                                            id="path-4"
                                            ></path>
                                        <path
                                            d="M20.6,7.13333333 L25.6,13.8 C26.2627417,14.6836556 26.0836556,15.9372583 25.2,16.6 C24.8538077,16.8596443 24.4327404,17 24,17 L14,17 C12.8954305,17 12,16.1045695 12,15 C12,14.5672596 12.1403557,14.1461923 12.4,13.8 L17.4,7.13333333 C18.0627417,6.24967773 19.3163444,6.07059163 20.2,6.73333333 C20.3516113,6.84704183 20.4862915,6.981722 20.6,7.13333333 Z"
                                            id="path-5"
                                            ></path>
                                    </defs>
                                    <g id="g-app-brand" stroke="none" stroke-width="1" fill="none" fill-rule="evenodd">
                                        <g id="Brand-Logo" transform="translate(-27.000000, -15.000000)">
                                            <g id="Icon" transform="translate(27.000000, 15.000000)">
                                                <g id="Mask" transform="translate(0.000000, 8.000000)">
                                                    <mask id="mask-2" fill="white">
                                                        <use xlink:href="#path-1"></use>
                                                    </mask>
                                                    <use fill="#696cff" xlink:href="#path-1"></use>
                                                    <g id="Path-3" mask="url(#mask-2)">
                                                        <use fill="#696cff" xlink:href="#path-3"></use>
                                                        <use fill-opacity="0.2" fill="#FFFFFF" xlink:href="#path-3"></use>
                                                    </g>
                                                    <g id="Path-4" mask="url(#mask-2)">
                                                        <use fill="#696cff" xlink:href="#path-4"></use>
                                                        <use fill-opacity="0.2" fill="#FFFFFF" xlink:href="#path-4"></use>
                                                    </g>
                                                </g>
                                                <g
                                                    id="Triangle"
                                                    transform="translate(19.000000, 11.000000) rotate(-300.000000) translate(-19.000000, -11.000000) "
                                                    >
                                                    <use fill="#696cff" xlink:href="#path-5"></use>
                                                    <use fill-opacity="0.2" fill="#FFFFFF" xlink:href="#path-5"></use>
                                                </g>
                                            </g>
                                        </g>
                                    </g>
                                </svg>
                            </span>
                            <span class="app-brand-text demo menu-text fw-bolder ms-2">Student</span>
                        </a>
                        <a href="javascript:void(0);" class="layout-menu-toggle menu-link text-large ms-auto d-block d-xl-none">
                        <i class="bx bx-chevron-left bx-sm align-middle"></i>
                        </a>
                    </div>
                    <div class="menu-inner-shadow"></div>
                    <ul class="menu-inner py-1">
                        <!-- Dashboard -->
                        <li class="menu-item active">
                            <a href="index.html" class="menu-link">
                                <i class="menu-icon tf-icons bx bx-home-circle"></i>
                                <div data-i18n="Analytics">Dashboard</div>
                            </a>
                        </li>
                        <!-- Layouts -->
                        <li class="menu-item open">
                            <a href="javascript:void(0);" class="menu-link menu-toggle">
                                <i class="menu-icon tf-icons bx bx-cube-alt"></i>
                                <div data-i18n="Layouts">Settings</div>
                            </a>
                            <ul class="menu-sub">
                                <li class="menu-item">
                                    <a href="{{ route('teachers.index') }}" class="menu-link">
                                        <div data-i18n="Without menu"><i class="menu-icon tf-icons bx bx-user"></i> Teachers</div>
                                    </a>
                                </li>
                                <li class="menu-item">
                                    <a href="{{ route('students.index') }}" class="menu-link">
                                        <div data-i18n="Without navbar"><i class="menu-icon tf-icons bx bx-user"></i> Students</div>
                                    </a>
                                </li>
                                <li class="menu-item">
                                    <a href="{{ route('subjects.index') }}" class="menu-link">
                                        <div data-i18n="Container"><i class="menu-icon tf-icons bx bx-cube-alt"></i> Subjects</div>
                                    </a>
                                </li>
                                <li class="menu-item">
                                    <a href="{{ route('terms.index') }}" class="menu-link">
                                        <div data-i18n="Fluid"><i class="menu-icon tf-icons bx bx-cube-alt"></i> Terms</div>
                                    </a>
                                </li>
                                 
                            </ul>
                        </li>
                       
                        <!-- Components -->
                        <li class="menu-header small text-uppercase"><span class="menu-header-text">Results</span></li>
                        <!-- Cards -->
                        <li class="menu-item">
                            <a href="{{ route('marks.index') }}" class="menu-link">
                                <i class="menu-icon tf-icons bx bx-collection"></i>
                                <div data-i18n="Basic">Marks</div>
                            </a>
                        </li>
                        
                    </ul>
                </aside>
                <!-- / Menu -->
                <!-- Layout container -->
                <div class="layout-page">
                    <!-- Navbar -->
                    <nav
                        class="layout-navbar container-xxl navbar navbar-expand-xl navbar-detached align-items-center bg-navbar-theme"
                        id="layout-navbar"
                        >
                        <div class="layout-menu-toggle navbar-nav align-items-xl-center me-3 me-xl-0 d-xl-none">
                            <a class="nav-item nav-link px-0 me-xl-4" href="javascript:void(0)">
                            <i class="bx bx-menu bx-sm"></i>
                            </a>
                        </div>
                        <div class="navbar-nav-right d-flex align-items-center" id="navbar-collapse">
                            <!-- Search -->
                            <div class="navbar-nav align-items-center">
                                <div class="nav-item d-flex align-items-center">
                                    <i class="bx bx-search fs-4 lh-0"></i>
                                    <input
                                        type="text"
                                        class="form-control border-0 shadow-none"
                                        placeholder="Search..."
                                        aria-label="Search..."
                                        />
                                </div>
                            </div>
                            <!-- /Search -->
                            <ul class="navbar-nav flex-row align-items-center ms-auto">
                                <!-- Place this tag where you want the button to render. --> 
                                <!-- User -->
                                <li class="nav-item navbar-dropdown dropdown-user dropdown">
                                    <a class="nav-link dropdown-toggle hide-arrow" href="javascript:void(0);" data-bs-toggle="dropdown">
                                        <div class="avatar avatar-online">
                                            <img src="{!! asset('dashboard/assets/img/avatars/1.png') !!}" alt class="w-px-40 h-auto rounded-circle" />
                                        </div>
                                    </a>
                                    <ul class="dropdown-menu dropdown-menu-end">
                                        <li>
                                            <a class="dropdown-item" href="#">
                                                <div class="d-flex">
                                                    <div class="flex-shrink-0 me-3">
                                                        <div class="avatar avatar-online">
                                                            <img src="{!! asset('dashboard/assets/img/avatars/1.png') !!}" alt class="w-px-40 h-auto rounded-circle" />
                                                        </div>
                                                    </div>
                                                    <div class="flex-grow-1">
                                                        <span class="fw-semibold d-block">{{ Auth::user()->name }}</span>
                                                        <small class="text-muted">{{ Auth::user()->email }}</small>
                                                    </div>
                                                </div>
                                            </a>
                                        </li>
                                        {{-- <li>
                                            <div class="dropdown-divider"></div>
                                        </li>
                                        <li>
                                            <a class="dropdown-item" href="#">
                                            <i class="bx bx-user me-2"></i>
                                            <span class="align-middle">My Profile</span>
                                            </a>
                                        </li> --}}
                                         
                                        <li>
                                            <div class="dropdown-divider"></div>
                                        </li>
                                        <li>
                                            <a class="dropdown-item" href="{{ route('logout') }}"
                                            onclick="event.preventDefault();
                                                          document.getElementById('logout-form').submit();">
                                             
                                            <i class="bx bx-power-off me-2"></i>
                                            <span class="align-middle">Log Out</span>
                                            </a>
                                            <form id="logout-form" action="{{ route('logout') }}" method="POST" class="d-none">
                                                @csrf
                                            </form>
                                        </li>
                                    </ul>
                                </li>
                                <!--/ User -->
                            </ul>
                        </div>
                    </nav>
                    <!-- / Navbar -->
                    <!-- Content wrapper -->
                    <div class="content-wrapper">
                        @yield('content') 
                        <!-- Footer -->
                        <footer class="content-footer footer bg-footer-theme">
                            <div class="container-xxl d-flex flex-wrap justify-content-between py-2 flex-md-row flex-column">
                                <div class="mb-2 mb-md-0">
                                    ©
                                    <script>
                                        document.write(new Date().getFullYear());
                                    </script>
                                    , made with ❤️ by
                                    <a href="https://themeselection.com" target="_blank" class="footer-link fw-bolder">rifassafir@gmail.com</a>
                                </div>
                                 
                            </div>
                        </footer>
                        <!-- / Footer -->
                        <div class="content-backdrop fade"></div>
                    </div>
                    <!-- Content wrapper -->
                </div>
                <!-- / Layout page -->
            </div>
            <!-- Overlay -->
            <div class="layout-overlay layout-menu-toggle"></div>
        </div>
        <div id="toasterSection">@include('admin.toast')</div>
        <div class="modal fade" id="deleteConfirmationPopUp" tabindex="-1" aria-labelledby="exampleModalLabel" aria-hidden="true">
            <div class="modal-dialog">
              <div class="modal-content">
                <div class="modal-header">
                  <h5 class="modal-title" id="exampleModalLabel">CONFIRM DELETE</h5>
                  <button type="button" class="btn-close" data-bs-dismiss="modal" aria-label="Close"></button>
                </div>
                <div class="modal-body">
                    Do you really want to delete ?
                </div>
                <div class="modal-footer">
                  <button type="button" class="btn btn-secondary" data-bs-dismiss="modal">NO</button>
                  <button type="button" class="btn btn-primary processDeleteAction">YES</button>
                  <form action="" id="deleteForm" method="post">
                        <input type="hidden" name="" id="deleteId" value="">
                        <input type="hidden" name="" id="deleteTable" value="">
                  </form>
                </div>
              </div>
            </div>
          </div>
        <!-- / Layout wrapper --> 
        <script src="{!! asset('dashboard/assets/vendor/libs/jquery/jquery.js') !!}"></script>
        <script src="{!! asset('dashboard/assets/vendor/libs/popper/popper.js') !!}"></script>
        <script src="{!! asset('dashboard/assets/vendor/js/bootstrap.js') !!}"></script>
        <script src="{!! asset('dashboard/assets/vendor/libs/perfect-scrollbar/perfect-scrollbar.js') !!}"></script>
        <script src="{!! asset('dashboard/assets/vendor/js/menu.js') !!}"></script> 
        <script src="{!! asset('dashboard/assets/vendor/libs/apex-charts/apexcharts.js') !!}"></script> 
        <script src="{!! asset('dashboard/assets/js/main.js') !!}"></script> 
        <script src="{!! asset('dashboard/assets/js/dashboards-analytics.js') !!}"></script> 
        <script src="{!! asset('dashboard/assets/js/ui-toasts.js') !!}"></script> 
        <script async defer src="https://buttons.github.io/buttons.js"></script>
        <!-- Custom Script Started -->
        <script>
            /*
            |--------------------------------------------------------------------------
            | Web Scripts : Student Management Script 
            |--------------------------------------------------------------------------
            |created at : 11 Nov 2021
            |created by : SAFIR S
            |
            */
            const siteURL       = "{{ URL::to('/') }}";
            const _token        = "{{ csrf_token() }}";
            const currentURL    = "{{ Route::current()->getName() }}";
            
            var Teacher = function ($) { 
                var createUser = function() {  
                    $(document).on('click','.createTeacherButton',function(e){
                        e.preventDefault();
                        var This = $(this);  
                        $.ajax({
                            url:"{{ route('teachers.store') }}", 
                            method:"POST",
                            beforeSend: function(){
                                This.text('Loading..');
                                $('.error-span').remove()
                            },
                            data:{
                                _token: "{{ csrf_token() }}",  
                                name: $('input[name="name"]').val(),   
                            },
                            success: function(data, status){   
                                This.text('Save');
                                This.closest('form')[0].reset();
                                let editURL =  siteURL+'/teachers/'+data.teacher.id+'/edit';
                                showToaster('bg-success','Success!',data.message);
                                $('.emptyTd').remove();
                                $('.teacherLoopSection').append(`<tr> 
                                <td>`+data.teacher.name+`</td> 
                                <td>
                                    <a class="btn btn-sm btn-primary" href="`+editURL+`" ><i class="bx bx-edit-alt me-1"></i> Edit</a >
                                    <a class="btn btn-sm btn-danger" href="" ><i class="bx bx-trash me-1"></i> Delete</a >
                                </td>
                            </tr>`);
                            },
                            error: function(error) {
                                This.text('Save');
                                let message = error.responseJSON.message;
                                let errors = error.responseJSON.errors;  
                                showToaster('bg-danger','Fields required.',message);
                                $.each(error.responseJSON.errors, function (i, error) {
                                    var el = $(document).find('[name="'+i+'"]');
                                    el.addClass('border-danger')
                                    el.after($('<span class="error-span" style="color: red;">'+error[0]+'</span>'));
                                }); 
                            }
                        });    
                    });
                    $(document).on('click','.createStudentButton',function(e){
                        e.preventDefault(); 
                        var This = $(this);  
                        $.ajax({
                            url:"{{ route('students.store') }}", 
                            method:"POST",
                            beforeSend: function(){
                                This.text('Loading..');
                                $('.error-span').remove()
                            },
                            data:{
                                _token: "{{ csrf_token() }}",  
                                name: $('input[name="name"]').val(),   
                                age: $('input[name="age"]').val(),   
                                teacher_id: $('select[name="teacher_id"]').val(),   
                                gender: $('select[name="gender"]').val(),   
                            },
                            success: function(data, status){   
                                This.text('Save');
                                This.closest('form')[0].reset();
                                let editURL =  siteURL+'/students/'+data.student.id+'/edit';
                                showToaster('bg-success','Success!',data.message);
                                $('.emptyTd').remove();
                                $('.studentLoopSection').append(`<tr> 
                                <td>`+data.student.name+`</td> 
                                <td>
                                    <a class="btn btn-sm btn-primary" href="`+editURL+`" ><i class="bx bx-edit-alt me-1"></i> Edit</a >
                                    <a class="btn btn-sm btn-danger" href="" ><i class="bx bx-trash me-1"></i> Delete</a >
                                </td>
                            </tr>`);
                            },
                            error: function(error) {
                                This.text('Save');
                                let message = error.responseJSON.message;
                                let errors = error.responseJSON.errors;  
                                showToaster('bg-danger','Fields required.',message);
                                $.each(error.responseJSON.errors, function (i, error) {
                                    var el = $(document).find('[name="'+i+'"]');
                                    el.addClass('border-danger')
                                    el.after($('<span class="error-span" style="color: red;">'+error[0]+'</span>'));
                                }); 
                            }
                        });    
                    });
                    $(document).on('click','.createSubjectButton',function(e){
                        e.preventDefault(); 
                        var This = $(this);  
                        $.ajax({
                            url:"{{ route('subjects.store') }}", 
                            method:"POST",
                            beforeSend: function(){
                                This.text('Loading..');
                                $('.error-span').remove()
                            },
                            data:{
                                _token: "{{ csrf_token() }}",  
                                name: $('input[name="name"]').val(),    
                            },
                            success: function(data, status){   
                                This.text('Save');
                                This.closest('form')[0].reset();
                                let editURL =  siteURL+'/subjects/'+data.subject.id+'/edit';
                                showToaster('bg-success','Success!',data.message);
                                $('.emptyTd').remove();
                                $('.subjectLoopSection').append(`<tr> 
                                <td>`+data.subject.name+`</td> 
                                <td>
                                    <a class="btn btn-sm btn-primary" href="`+editURL+`" ><i class="bx bx-edit-alt me-1"></i> Edit</a >
                                    <a class="btn btn-sm btn-danger" href="" ><i class="bx bx-trash me-1"></i> Delete</a >
                                </td>
                            </tr>`);
                            },
                            error: function(error) {
                                This.text('Save');
                                let message = error.responseJSON.message;
                                let errors = error.responseJSON.errors;  
                                showToaster('bg-danger','Fields required.',message);
                                $.each(error.responseJSON.errors, function (i, error) {
                                    var el = $(document).find('[name="'+i+'"]');
                                    el.addClass('border-danger')
                                    el.after($('<span class="error-span" style="color: red;">'+error[0]+'</span>'));
                                }); 
                            }
                        });    
                    });
                    $(document).on('click','.createTermButton',function(e){
                        e.preventDefault(); 
                        var This = $(this);  
                        $.ajax({
                            url:"{{ route('terms.store') }}", 
                            method:"POST",
                            beforeSend: function(){
                                This.text('Loading..');
                                $('.error-span').remove()
                            },
                            data:{
                                _token: "{{ csrf_token() }}",  
                                name: $('input[name="name"]').val(),    
                            },
                            success: function(data, status){   
                                This.text('Save');
                                This.closest('form')[0].reset();
                                let editURL =  siteURL+'/terms/'+data.term.id+'/edit';
                                showToaster('bg-success','Success!',data.message);
                                $('.emptyTd').remove();
                                $('.termLoopSection').append(`<tr> 
                                <td>`+data.term.name+`</td> 
                                <td>
                                    <a class="btn btn-sm btn-primary" href="`+editURL+`" ><i class="bx bx-edit-alt me-1"></i> Edit</a >
                                    <a class="btn btn-sm btn-danger" href="" ><i class="bx bx-trash me-1"></i> Delete</a >
                                </td>
                            </tr>`);
                            },
                            error: function(error) {
                                This.text('Save');
                                let message = error.responseJSON.message;
                                let errors = error.responseJSON.errors;  
                                showToaster('bg-danger','Fields required.',message);
                                $.each(error.responseJSON.errors, function (i, error) {
                                    var el = $(document).find('[name="'+i+'"]');
                                    el.addClass('border-danger')
                                    el.after($('<span class="error-span" style="color: red;">'+error[0]+'</span>'));
                                }); 
                            }
                        });    
                    });


                }    
                var deleteAction = function(){
                    $(document).on('click','.deleteActionbutton',function(e){
                        e.preventDefault();
                        $('#deleteConfirmationPopUp').modal('show'); 
                        var This =$(this);
                        $('#deleteId').val(This.data('id'));
                        $('#deleteTable').val(This.data('table'));
                    });
                    $(document).on('click','.processDeleteAction',function(e){
                        e.preventDefault();
                        let deleteId = $('#deleteId').val();
                        let deleteTable = $('#deleteTable').val();
                        $(this).text('Deleting..');
                        $.ajax({
                            url: siteURL+"/"+deleteTable+"/"+deleteId, 
                            method:"DELETE", 
                            data:{
                                _token: "{{ csrf_token() }}",   
                            },
                            success: function(data, status){   
                                showToaster('bg-success','Success!',data.message);
                                setTimeout(() => {
                                    location.reload();
                                }, 1000);
                            },
                            error: function(error) {
                               
                            }
                        });   
                    });
                }
                return {
                    init : function () { 
                        createUser();   
                        deleteAction();
                    }
                }
            
            } (jQuery)
            Teacher.init();
            var showToaster = function(type='bg-success',title='',message='') { 
                    $('#toaster_title').html(title);
                    $('#toaster_content').html(message);
                    const toastPlacementExample = document.querySelector('.toast-placement-ex')
                    toastPlacementExample.classList.add(type);
                    DOMTokenList.prototype.add.apply(toastPlacementExample.classList,['top-0','end-0']);
                    toastPlacement = new bootstrap.Toast(toastPlacementExample);
                    toastPlacement.show();
            }


            $(document).on('click','.createMarkButton',function(e){
                        e.preventDefault(); 
                        var This = $(this);   
                        subjectMarkArraySet = []; 
                        $('.subject_id').each(function(i, el){ 
                            subjectMarkArraySet.push({
                                subject_id: $(this).val(),
                                mark: $('.marks')[0].value, 
                            }); 
                        });
                        $.ajax({
                            url:"{{ route('marks.store') }}", 
                            method:"POST",
                            beforeSend: function(){
                                This.text('Loading..');
                                $('.error-span').remove()
                            },
                            data: { 
                                _token:_token,
                                 student_id:$('select[name="student_id"]').val(),
                                 term_id:$('select[name="term_id"]').val(),
                                 subject_mark_set:subjectMarkArraySet,
                            },
                            success: function(data, status){   
                                This.text('Save');
                                This.closest('form')[0].reset();
                                let editURL =  siteURL+'/subjects/'+data.subject.id+'/edit';
                                showToaster('bg-success','Success!',data.message);
                                $('.emptyTd').remove();
                                $('.subjectLoopSection').append(`<tr> 
                                <td>`+data.subject.name+`</td> 
                                <td>
                                    <a class="btn btn-sm btn-primary" href="`+editURL+`" ><i class="bx bx-edit-alt me-1"></i> Edit</a >
                                    <a class="btn btn-sm btn-danger" href="" ><i class="bx bx-trash me-1"></i> Delete</a >
                                </td>
                            </tr>`);
                            },
                            error: function(error) {
                                This.text('Save');
                                let message = error.responseJSON.message;
                                let errors = error.responseJSON.errors;  
                                showToaster('bg-danger','Fields required.',message);
                                $.each(error.responseJSON.errors, function (i, error) {
                                    var el = $(document).find('[name="'+i+'"]');
                                    el.addClass('border-danger')
                                    el.after($('<span class="error-span" style="color: red;">'+error[0]+'</span>'));
                                }); 
                            }
                        });    
                    });
        </script>
        <!-- Custom Script Ends -->
    </body>
</html>
