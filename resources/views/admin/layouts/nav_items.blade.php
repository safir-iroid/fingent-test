<ul class="nav nav-pills flex-column flex-md-row mb-3">
    <li class="nav-item">
        <a class="nav-link {{ ((Request::segment(1) == 'teachers') ? 'active' : '') }}" href="{{ route('teachers.index') }}"><i class="bx bx-user me-1"></i> Teachers</a>
    </li>
    <li class="nav-item">
        <a class="nav-link {{ ((Request::segment(1) == 'students') ? 'active' : '') }}" href="{{ route('students.index') }}"
            ><i class="bx bx-user me-1"></i> Students</a
            >
    </li>
    <li class="nav-item">
        <a class="nav-link {{ ((Request::segment(1) == 'subjects') ? 'active' : '') }}" href="{{ route('subjects.index') }}"
            ><i class="bx bx-collection me-1"></i> Subjects</a
            >
    </li>
    <li class="nav-item">
        <a class="nav-link {{ ((Request::segment(1) == 'terms') ? 'active' : '') }}" href="{{ route('terms.index') }}"
            ><i class="bx bx-collection me-1"></i> Terms</a
            >
    </li>
</ul>