<?php

namespace App\Http\Controllers;

use App\Models\Term;
use Illuminate\Http\Request;
use App\Http\Requests\TermRequest;
use Redirect;
class TermController extends Controller
{
    /**
     * Display a listing of the resource.
     *
     * @return \Illuminate\Http\Response
     */
    public function index()
    {
        $termsList = Term::get();
        return view('admin.terms',compact('termsList'));
    }

    /**
     * Show the form for creating a new resource.
     *
     * @return \Illuminate\Http\Response
     */
    public function create()
    {
        // 
    }

    /**
     * Store a newly created resource in storage.
     *
     * @param  \Illuminate\Http\Request  $request
     * @return \Illuminate\Http\Response
     */
    public function store(TermRequest $request)
    { 
        try {
            $term = new Term();
            $term->name = $request->name;
            $term->save();
            return response()->json(['status' =>true,'message'=>'Successfully saved', 'term'=>$term]);
        }catch(\Exception $e)
        {
            return response()->json(['status'=>false ,'message'=>'Internal server Error']);
        }
    }

    /**
     * Display the specified resource.
     *
     * @param  \App\Models\Term  $term
     * @return \Illuminate\Http\Response
     */
    public function show(Term $term)
    { 
        //
    }

    /**
     * Show the form for editing the specified resource.
     *
     * @param  \App\Models\term  $term
     * @return \Illuminate\Http\Response
     */
    public function edit($id)
    { 
        $term = Term::find($id); 
        $termsList = Term::get();

        return view('admin.terms',compact('term','termsList'));
    }

    /**
     * Update the specified resource in storage.
     *
     * @param  \Illuminate\Http\Request  $request
     * @param  \App\Models\Term  $term
     * @return \Illuminate\Http\Response
     */
    public function update(Request $request, Term $term)
    { 

        $term->name  = $request->term;
        $term->save();
        return redirect()->back()->with('msg', 'successfully updated!');
    }

    /**
     * Remove the specified resource from storage.
     *
     * @param  \App\Models\term  $term
     * @return \Illuminate\Http\Response
     */
    public function destroy(Term $term)
    {
        Term::find($term->id)->delete($term->id);
        return response()->json([
            'message' => 'Record deleted successfully!'
        ]);
    }
}
