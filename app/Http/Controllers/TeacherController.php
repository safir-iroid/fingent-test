<?php

namespace App\Http\Controllers;

use App\Models\Teacher;
use Illuminate\Http\Request;
use App\Http\Requests\TeacherRequest;
use Redirect;
class TeacherController extends Controller
{
    /**
     * Display a listing of the resource.
     *
     * @return \Illuminate\Http\Response
     */
    public function index()
    {
        $teachersList = Teacher::get();
        return view('admin.teachers',compact('teachersList'));
    }

    /**
     * Show the form for creating a new resource.
     *
     * @return \Illuminate\Http\Response
     */
    public function create()
    {
        // 
    }

    /**
     * Store a newly created resource in storage.
     *
     * @param  \Illuminate\Http\Request  $request
     * @return \Illuminate\Http\Response
     */
    public function store(TeacherRequest $request)
    { 
        try {
            $teacher = new Teacher();
            $teacher->name = $request->name;
            $teacher->save();
            return response()->json(['status' =>true,'message'=>'Successfully saved', 'teacher'=>$teacher]);
        }catch(\Exception $e)
        {
            return response()->json(['status'=>false ,'message'=>'Internal server Error']);
        }
    }

    /**
     * Display the specified resource.
     *
     * @param  \App\Models\Teacher  $teacher
     * @return \Illuminate\Http\Response
     */
    public function show(Teacher $teacher)
    { 
        //
    }

    /**
     * Show the form for editing the specified resource.
     *
     * @param  \App\Models\Teacher  $teacher
     * @return \Illuminate\Http\Response
     */
    public function edit($id)
    { 
        $teacher = Teacher::find($id); 
        $teachersList = Teacher::get();

        return view('admin.teachers',compact('teacher','teachersList'));
    }

    /**
     * Update the specified resource in storage.
     *
     * @param  \Illuminate\Http\Request  $request
     * @param  \App\Models\Teacher  $teacher
     * @return \Illuminate\Http\Response
     */
    public function update(Request $request, Teacher $teacher)
    { 

        $teacher->name  = $request->teacher;
        $teacher->save();
        return redirect()->back()->with('msg', 'successfully updated!');
    }

    /**
     * Remove the specified resource from storage.
     *
     * @param  \App\Models\Teacher  $teacher
     * @return \Illuminate\Http\Response
     */
    public function destroy(Teacher $teacher)
    {
        Teacher::find($teacher->id)->delete($teacher->id);
        return response()->json([
            'message' => 'Record deleted successfully!'
        ]);
    }
}
