<?php

namespace App\Http\Controllers;
use App\Models\Mark;
use App\Models\MarkItem;
use App\Models\Subject;
use App\Models\Student;
use App\Models\Term;
use Illuminate\Http\Request;
use App\Http\Requests\MarkRequest;
use Redirect;

class MarkController extends Controller
{
    /**
     * Display a listing of the resource.
     *
     * @return \Illuminate\Http\Response
     */
    public function index()
    {
        $marksList = Mark::get();
        $subjects = Subject::get();
        $terms = Term::get();
        $students = Student::get(); 
        return view('admin.marks',compact('marksList','subjects','students','terms'));
    }

    /**
     * Show the form for creating a new resource.
     *
     * @return \Illuminate\Http\Response
     */
    public function create()
    {
        // 
    }

    /**
     * Store a newly created resource in storage.
     *
     * @param  \Illuminate\Http\Request  $request
     * @return \Illuminate\Http\Response
     */
    public function store(Request $request)
    { 
        try {
            $mark = new Mark();
            $mark->student_id = $request->student_id;
            $mark->term_id = $request->term_id;
            $mark->save();

            foreach($request->subject_mark_set as $value){
                $markItem = new MarkItem();
                $markItem->mark_id = $mark->id;
                $markItem->subject_id = $value['subject_id'];
                $markItem->marks = $value['mark'];
                $markItem->save();
            }
             
            return response()->json(['status' =>true,'message'=>'Successfully saved', 'mark'=>$mark]);
        }catch(\Exception $e)
        {
            return response()->json(['status'=>false ,'message'=>'Internal server Error'.$e->getMessage()]);
        }
    }

    /**
     * Display the specified resource.
     *
     * @param  \App\Models\Mark  $mark
     * @return \Illuminate\Http\Response
     */
    public function show(Mark $mark)
    { 
        //
    }

    /**
     * Show the form for editing the specified resource.
     *
     * @param  \App\Models\Mark  $mark
     * @return \Illuminate\Http\Response
     */
    public function edit($id)
    { 
        $mark = Mark::find($id); 
        $marksList = Mark::get();
        $subjects = Subject::get();
        $terms = Term::get();
        $students = Student::get();  
        return view('admin.marks',compact('mark','marksList','subjects','terms','students'));
    }

    /**
     * Update the specified resource in storage.
     *
     * @param  \Illuminate\Http\Request  $request
     * @param  \App\Models\Mark  $mark
     * @return \Illuminate\Http\Response
     */
    public function update(Request $request, mark $mark)
    { 

        $mark->name  = $request->mark;
        $mark->save();
        return redirect()->back()->with('msg', 'successfully updated!');
    }

    /**
     * Remove the specified resource from storage.
     *
     * @param  \App\Models\mark  $mark
     * @return \Illuminate\Http\Response
     */
    public function destroy(Mark $mark)
    {
        Mark::find($mark->id)->delete();
        MarkItem::where('mark_id',$mark->id)->delete();
        return response()->json([
            'message' => 'Record deleted successfully!'
        ]);
    }
}
