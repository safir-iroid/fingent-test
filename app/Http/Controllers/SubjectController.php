<?php

namespace App\Http\Controllers;

use App\Models\Subject;
use Illuminate\Http\Request;
use App\Http\Requests\SubjectRequest;
use Redirect;
class SubjectController extends Controller
{
    /**
     * Display a listing of the resource.
     *
     * @return \Illuminate\Http\Response
     */
    public function index()
    {
        $subjectsList = Subject::get();
        return view('admin.subjects',compact('subjectsList'));
    }

    /**
     * Show the form for creating a new resource.
     *
     * @return \Illuminate\Http\Response
     */
    public function create()
    {
        // 
    }

    /**
     * Store a newly created resource in storage.
     *
     * @param  \Illuminate\Http\Request  $request
     * @return \Illuminate\Http\Response
     */
    public function store(SubjectRequest $request)
    { 
        try {
            $subject = new Subject();
            $subject->name = $request->name;
            $subject->save();
            return response()->json(['status' =>true,'message'=>'Successfully saved', 'subject'=>$subject]);
        }catch(\Exception $e)
        {
            return response()->json(['status'=>false ,'message'=>'Internal server Error']);
        }
    }

    /**
     * Display the specified resource.
     *
     * @param  \App\Models\Subject  $subject
     * @return \Illuminate\Http\Response
     */
    public function show(Subject $subject)
    { 
        //
    }

    /**
     * Show the form for editing the specified resource.
     *
     * @param  \App\Models\Subject  $subject
     * @return \Illuminate\Http\Response
     */
    public function edit($id)
    { 
        $subject = Subject::find($id); 
        $subjectsList = Subject::get();

        return view('admin.subjects',compact('subject','subjectsList'));
    }

    /**
     * Update the specified resource in storage.
     *
     * @param  \Illuminate\Http\Request  $request
     * @param  \App\Models\Subject  $subject
     * @return \Illuminate\Http\Response
     */
    public function update(Request $request, Subject $subject)
    { 

        $subject->name  = $request->subject;
        $subject->save();
        return redirect()->back()->with('msg', 'successfully updated!');
    }

    /**
     * Remove the specified resource from storage.
     *
     * @param  \App\Models\subject  $subject
     * @return \Illuminate\Http\Response
     */
    public function destroy(subject $subject)
    {
        Subject::find($subject->id)->delete($subject->id);
        return response()->json([
            'message' => 'Record deleted successfully!'
        ]);
    }
}
