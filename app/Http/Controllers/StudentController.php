<?php

namespace App\Http\Controllers;

use App\Models\Student;
use App\Models\Teacher;
use Illuminate\Http\Request;
use App\Http\Requests\StudentRequest;
use Redirect;

class StudentController extends Controller
{
    /**
     * Display a listing of the resource.
     *
     * @return \Illuminate\Http\Response
     */
    public function index()
    {
        $studentsList = Student::get();
        $teachers= Teacher::get();
        
        return view('admin.students',compact('studentsList','teachers')); 
    }

    /**
     * Show the form for creating a new resource.
     *
     * @return \Illuminate\Http\Response
     */
    public function create()
    {
        //
    }

    /**
     * Store a newly created resource in storage.
     *
     * @param  \Illuminate\Http\Request  $request
     * @return \Illuminate\Http\Response
     */
    public function store(StudentRequest $request)
    {
         
        try {
            $student = new Student();
            $student->name = $request->name;
            $student->age = $request->age;
            $student->gender = $request->gender;
            $student->teacher_id = $request->teacher_id;
            $student->save();
            return response()->json(['status' =>true,'message'=>'Successfully saved', 'student'=>$student]);
        }catch(\Exception $e)
        {
            return response()->json(['status'=>false ,'message'=>$e]);
        }
    }

    /**
     * Display the specified resource.
     *
     * @param  \App\Models\Student  $student
     * @return \Illuminate\Http\Response
     */
    public function show(Student $student)
    {
        //
    }

    /**
     * Show the form for editing the specified resource.
     *
     * @param  \App\Models\Student  $student
     * @return \Illuminate\Http\Response
     */
    public function edit($id)
    {
        $student = Student::find($id); 
        $studentsList = Student::get();
        $teachers = Teacher::get();
        return view('admin.students',compact('student','studentsList','teachers'));
    }

    /**
     * Update the specified resource in storage.
     *
     * @param  \Illuminate\Http\Request  $request
     * @param  \App\Models\Student  $student
     * @return \Illuminate\Http\Response
     */
    public function update(Request $request, Student $student)
    {
        $student->name  = $request->name;
        $student->age  = $request->age;
        $student->gender  = $request->gender;
        $student->teacher_id  = $request->teacher_id;
        $student->save();
        return redirect()->back()->with('msg', 'successfully updated!');
    }

    /**
     * Remove the specified resource from storage.
     *
     * @param  \App\Models\Student  $student
     * @return \Illuminate\Http\Response
     */
    public function destroy(Student $student)
    { 
        Student::find($student->id)->delete($student->id);
        return response()->json([
            'message' => 'Record deleted successfully!'
        ]);
    }
}
