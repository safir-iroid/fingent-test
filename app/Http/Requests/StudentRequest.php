<?php

namespace App\Http\Requests;

use Illuminate\Foundation\Http\FormRequest;

class StudentRequest extends FormRequest
{
    /**
     * Determine if the user is authorized to make this request.
     *
     * @return bool
     */
    public function authorize()
    {
        return true;
    }

    /**
     * Get the validation rules that apply to the request.
     *
     * @return array<string, mixed>
     */
    public function rules()
    {
        return [
            "name"=>"required",
            "age"=>"required",
            "gender"=>"required",
            "teacher_id"=>"required",
        ]; 
    }
    public function messages()
    {
        return [
            "name.required"=>"Name is required.",
            "age.required"=>"Age is required.",
            "gender.required"=>"Gender is required.",
            "teacher_id.required"=>"Teacher is required."
        ];
    }
    public function validate() {
        $instance = $this->getValidatorInstance();
        if ($instance->fails()) {
            throw new HttpResponseException(response()->json($instance->errors(), 422));
        }
    }
}
